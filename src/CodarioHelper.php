<?php

namespace Drupal\codario;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\TempStore\SharedTempStoreFactory;
use GuzzleHttp\Client;
use Drupal\Core\Extension\ModuleExtensionList;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\RequestOptions;

/**
 * Methods that operate with Codario.
 */
class CodarioHelper {

  /**
   * Shared Temp Store Factory.
   *
   * @var \Drupal\Core\TempStore\SharedTempStoreFactory
   */
  protected SharedTempStoreFactory $tempStoreFactory;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected ConfigFactory $configFactory;

  /**
   * HTTP Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected Client $httpClient;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected DateFormatter $dateFormatter;

  /**
   * Module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected ModuleExtensionList $moduleExtensionList;

  /**
   * Theme extension list.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  protected ThemeExtensionList $themeExtensionList;

  /**
   * Target URL property.
   */
  public const PROPERTY_TARGET_URL = 'target_url';

  /**
   * Customer secret property.
   */
  public const PROPERTY_CUSTOMER_SECRET = 'customer_secret';

  /**
   * Project ID property.
   */
  public const PROPERTY_PROJECT_ID = 'project_id';

  /**
   * Connection is ok.
   */
  public const CONNECTION_STATUS_CONNECTED = 'connected';

  /**
   * Connection is failed.
   */
  public const CONNECTION_STATUS_FAILED = 'failed';

  /**
   * Helper constructor.
   *
   * @param \Drupal\Core\TempStore\SharedTempStoreFactory $tempStoreFactory
   *   The shared Temp Store Factory.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config Factory.
   * @param \GuzzleHttp\Client $httpClient
   *   The config Factory.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   The date formatter.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   Module extension list.
   * @param \Drupal\Core\Extension\ThemeExtensionList $themeExtensionList
   *   Theme extension list.
   */
  public function __construct(SharedTempStoreFactory $tempStoreFactory, ConfigFactory $configFactory, Client $httpClient, DateFormatter $dateFormatter, ModuleExtensionList $moduleExtensionList, ThemeExtensionList $themeExtensionList) {
    $this->tempStoreFactory = $tempStoreFactory;
    $this->configFactory = $configFactory;
    $this->httpClient = $httpClient;
    $this->dateFormatter = $dateFormatter;
    $this->moduleExtensionList = $moduleExtensionList;
    $this->themeExtensionList = $themeExtensionList;
  }

  /**
   * Check configs defined.
   *
   * @return boolean
   *   Is configs defined.
   */
  public function isConfigsDefined() {
    return !!$this->configFactory->get('codario.settings')->get(static::PROPERTY_TARGET_URL);
  }

  /**
   * Pass core/modules to Target URL.
   */
  public function sendDataToTargetServer() {
    $configs = $this->configFactory->get('codario.settings');
    $dependencies = [];

    $extensions = array_merge($this->moduleExtensionList->getList(), $this->themeExtensionList->getList());
    foreach ($extensions as $extension) {
      $dependencies[] = [
        'id' => $extension->getName(),
        'version' => $extension->info['version'] ?? 'unknown',
        'type' => $extension->getType(),
        'active' => (bool) $extension->status,
        'name' => $extension->getName(),
        'raw' => [],
      ];
    }

    $body = [
      'coreInfo' => [
        'targetURL' => $configs->get(static::PROPERTY_TARGET_URL),
        'customerSecret' => $configs->get(static::PROPERTY_CUSTOMER_SECRET),
        'projectId' => $configs->get(static::PROPERTY_PROJECT_ID),
      ],
      'systemInfo' => [
        'coreSystem' => 'drupal',
        'coreVersion' => \Drupal::VERSION,
        'runTime' => 'php',
        'runTimeVersion' => phpversion(),
        'applicationURL' => \Drupal::request()->getSchemeAndHttpHost(),
      ],
      'dependencies' => $dependencies,
    ];

    $target = sprintf('%s/plugin/project/%s', $configs->get(static::PROPERTY_TARGET_URL), $configs->get(static::PROPERTY_PROJECT_ID));

    try {
      $response = $this->httpClient->post($target, [
        'form_params' => $body,
      ]);

      $statusCode = $response->getStatusCode();

      if ($statusCode !== 200) {
        $this->tempStoreFactory->get('codario')->set('message', $response->getBody()->getContents());
      }
    } catch (TransferException $e) {
      $this->tempStoreFactory->get('codario')->set('error_message', 'Connection refused (code ' . $e->getCode() . ')');
      $statusCode = 0;
    }

    $date = \Drupal::service('date.formatter')->format(\Drupal::time()->getRequestTime(), 'long');
    $this->tempStoreFactory->get('codario')->set('last_send_date', $date);
    $this->tempStoreFactory->get('codario')->set('status', $statusCode === 200 ? static::CONNECTION_STATUS_CONNECTED : static::CONNECTION_STATUS_FAILED);
  }

  /**
   * Pass core/modules to Target URL.
   */
  public function getConnectionStatus() {
    return [
      'status' => $this->tempStoreFactory->get('codario')->get('status'),
      'error_message' => $this->tempStoreFactory->get('codario')->get('error_message'),
      'last_send_date' => $this->tempStoreFactory->get('codario')->get('last_send_date'),
    ];
  }
}
