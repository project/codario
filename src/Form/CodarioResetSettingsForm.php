<?php

namespace Drupal\codario\Form;

use Drupal\codario\CodarioHelper;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Routing\RedirectDestination;
use Drupal\Core\Url;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for Codario module.
 */
class CodarioResetSettingsForm extends ConfirmFormBase {

  /**
   * The menu link manager instance.
   *
   * @var \Drupal\codario\CodarioHelper
   */
  protected CodarioHelper $codarioHelper;

  /**
   * AdminToolbarToolsSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\codario\CodarioHelper $codarioHelper
   *   The Codario helper.
   */
  public function __construct(CodarioHelper $codarioHelper) {
    $this->codarioHelper = $codarioHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('codario.helper.main'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin_codario_settings';
  }
  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('codario.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to reset Codario settings?');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this
      ->configFactory()
      ->getEditable('codario.settings')
      ->delete();

    $form_state->setRedirect('codario.settings');
  }

}
