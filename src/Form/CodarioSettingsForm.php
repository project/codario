<?php

namespace Drupal\codario\Form;

use Drupal\codario\CodarioHelper;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Environment;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for Codario module.
 */
class CodarioSettingsForm extends ConfigFormBase {

  /**
   * The menu link manager instance.
   *
   * @var \Drupal\codario\CodarioHelper
   */
  protected CodarioHelper $codarioHelper;

  /**
   * AdminToolbarToolsSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\codario\CodarioHelper $codarioHelper
   *   The Codario helper.
   */
  public function __construct(ConfigFactoryInterface $configFactory, CodarioHelper $codarioHelper) {
    parent::__construct($configFactory);
    $this->codarioHelper = $codarioHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('codario.helper.main'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'codario.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin_codario_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('codario.settings');

    $validators = [
      'file_validate_extensions' => ['json'],
      'file_validate_size' => [Environment::getUploadMaxSize()],
    ];

    $form['tabs']['current_connection'] = [
      '#type' => 'details',
      '#title' => 'Connection details',
      '#open' => TRUE,
    ];

    $status = $this->codarioHelper->getConnectionStatus();

    $form['tabs']['current_connection']['current_status'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => $this->t('Status'),
    ];

    $color = $status['status'] === CodarioHelper::CONNECTION_STATUS_CONNECTED ? '#228B22' : '#B22222';
    $form['tabs']['current_connection']['status'] = [
      '#type' => 'markup',
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $this->t('Connection status: <strong style="color:@color">@status</strong>', [
        '@color' => $color,
        '@status' => $status['status'],
      ]),
    ];

    if ($status['status'] === CodarioHelper::CONNECTION_STATUS_FAILED) {
      $form['tabs']['current_connection']['error_message'] = [
        '#type' => 'markup',
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => $this->t('The problem:') . ' <strong>' . ($status['error_message'] ?? $this->t('unknown')) . '</strong>',
      ];
    }

    $form['tabs']['current_connection']['last_send_date'] = [
      '#type' => 'markup',
      '#prefix' => '<p><small>',
      '#suffix' => '</small></p>',
      '#markup' => $this->t('Last data was sent:') . ' <strong>' . ($status['last_send_date'] ?? $this->t('never')) . '</strong>',
    ];

    $form['tabs']['current_connection']['line1'] = [
      '#type' => 'html_tag',
      '#tag' => 'hr',
    ];

    $form['tabs']['current_connection']['current_configs'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => $this->t('Configs'),
    ];

    $form['tabs']['current_connection']['target_url'] = [
      '#type' => 'markup',
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $this->t('Target URL: <i>@value</i>', [
        '@value' => $config->get(CodarioHelper::PROPERTY_TARGET_URL),
      ]),
    ];

    $form['tabs']['current_connection']['customer_secret'] = [
      '#type' => 'markup',
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $this->t('Customer Secret: <i>@value</i>', [
        '@value' => substr_replace($config->get(CodarioHelper::PROPERTY_CUSTOMER_SECRET) ?? '', str_repeat('*', 50), -55, 50),
      ]),
    ];

    $form['tabs']['current_connection']['project_id'] = [
      '#type' => 'markup',
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $this->t('Project ID: <i>@value</i>', [
        '@value' => $config->get(CodarioHelper::PROPERTY_PROJECT_ID),
      ]),
    ];

    $form['tabs']['current_connection']['line2'] = [
      '#type' => 'html_tag',
      '#tag' => 'hr',
    ];

    $form['tabs']['current_connection']['reset'] = [
      '#type' => 'link',
      '#attributes' => [
        'class' => [
          'button',
          'button--danger',
        ],
      ],
      '#title' => $this->t('Reset configs'),
      '#url' => Url::fromRoute('codario.reset'),
    ];

    $form['tabs']['update_configs'] = [
      '#type' => 'details',
      '#title' => !$this->codarioHelper->isConfigsDefined() ? t('Establish connection') : t('Update connection configs'),
      '#open' => !$this->codarioHelper->isConfigsDefined(),
    ];

    $form['tabs']['update_configs']['configs_file'] = [
      '#type' => 'managed_file',
      '#name' => 'configs_file',
      '#title' => t('Codario configs file'),
      '#size' => 20,
      '#required' => TRUE,
      '#upload_validators' => $validators,
      '#description' => $this->t('Upload the generated on app.codario.io configs file'),
      '#upload_location' => 'private://codario/',
    ];

    if (!$this->codarioHelper->isConfigsDefined()) {
      unset($form['tabs']['current_connection']);
    }

    $form = parent::buildForm($form, $form_state);
    $actions = $form['actions'];
    unset($form['actions']);

    $actions['submit']['#value'] = $this->t('Establish connection');
    $form['tabs']['update_configs']['actions'] = $actions;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $input = $form_state->getUserInput();

    // Skip the validation for "deletion" case.
    if (!empty($input['_triggering_element_name']) && $input['_triggering_element_name'] === 'configs_file_remove_button') {
      return TRUE;
    }

    $configs = $this->getConfigs($form_state);

    if (empty($configs) || !is_array($configs)) {
      $form_state->setErrorByName('configs_file', $this->t('The uploaded file contains incorrect data.'));
      return FALSE;
    }

    $keys = [
      'targetUrl',
      'customerSecret',
      'projectId',
    ];

    foreach ($keys as $key) {
      if (empty($configs[$key]) || !is_string($configs[$key])) {
        $form_state->setErrorByName('configs_file', $this->t('The uploaded file contains incorrect data.'));
        return FALSE;
      }
    }

    if (count($keys) !== count($configs)) {
      $form_state->setErrorByName('configs_file', $this->t('The uploaded file contains incorrect data.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fileId = $form_state->getValue('configs_file')[0];
    $file = File::load($fileId);
    $configs = Json::decode(file_get_contents(\Drupal::service('file_system')->realpath($file->getFileUri())));
    $file->delete();

    $this->config('codario.settings')
      ->set(CodarioHelper::PROPERTY_TARGET_URL, $configs['targetUrl'])
      ->set(CodarioHelper::PROPERTY_CUSTOMER_SECRET, $configs['customerSecret'])
      ->set(CodarioHelper::PROPERTY_PROJECT_ID, $configs['projectId'])
      ->save();

    $this->codarioHelper->sendDataToTargetServer();

    $this->messenger()->addStatus($this->t('Codario integration settings have been saved.'));
  }

  /**
   * {@inheritdoc}
   */
  private function getConfigs(FormStateInterface $form_state) {
    $fileValue = $form_state->getValue('configs_file');

    if (empty($fileValue)) {
      return NULL;
    }

    $fileId = $fileValue[0];

    $file = File::load($fileId);

    return Json::decode(file_get_contents(\Drupal::service('file_system')->realpath($file->getFileUri())));
  }

}
