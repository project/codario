# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.3]
- Update the format of passed data.

## [1.0.2]
- Change entrypoint URL.

## [1.0.1]
- Get rid of simple_cron dependency.
- Add D10 as supported core.

## [1.0.0]
- Initial release.
